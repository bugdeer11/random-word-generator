#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <cstring>

using namespace std;

string word_head_generate () {
    string word = "";
    string alf = "+����������������������������";

    int i = rand() % 30 + 1;

    word = alf[i];
    return word;
}

int string_length_counter (string alf) {
    int counter = 0;
    int mover = 1;
    string end_symbol = "-";

    while (alf[mover] != end_symbol[0]) {
        counter++;
        mover++;
    }
    //counter = counter - 1;
return counter;
}

string new_letter_generate_inside_function (string alf_for_letter){
    int string_length = 0;
    int i = 0;
    string letter;

    string_length = string_length_counter(alf_for_letter);

    i = rand() % string_length + 1;

    letter = alf_for_letter[i];

    return letter;
}

int letter_pozition_in_alf (string letter) {
    int letter_pozition = 1;
    string alf = "+��������������������������������";

    while (alf[letter_pozition] != letter[0]) {
        letter_pozition++;
    }
    return letter_pozition;
}

string new_letter_generate_outside_function (string letter_before ) {
    int letter_poz = 0;

    letter_poz = letter_pozition_in_alf(letter_before);

    switch (letter_poz) {
        case 1: letter_before = new_letter_generate_inside_function("+��������������������������-"); break; //�
        case 2: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�
        case 3: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�
        case 4: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�
        case 5: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�
        case 6: letter_before = new_letter_generate_inside_function("+�������������������������-"); break; //�
        case 7: letter_before = new_letter_generate_inside_function("+�������������������������-"); break; //�
        case 8: letter_before = new_letter_generate_inside_function("+������������������������-"); break; //�
        case 9: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�
        case 10: letter_before = new_letter_generate_inside_function("+�������������������������-"); break; //�

        case 11: letter_before = new_letter_generate_inside_function("+����������������������������-"); break; //�
        case 12: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�
        case 13: letter_before = new_letter_generate_inside_function("+�������������������������������-"); break; //�
        case 14: letter_before = new_letter_generate_inside_function("+�������������������������������-"); break; //�
        case 15: letter_before = new_letter_generate_inside_function("+�������������������������������-"); break; //�
        case 16: letter_before = new_letter_generate_inside_function("+�����������������������������-"); break; //�
        case 17: letter_before = new_letter_generate_inside_function("+�����������������������������-"); break; //�
        case 18: letter_before = new_letter_generate_inside_function("+�������������������������������-"); break; //�
        case 19: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�
        case 20: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�

        case 21: letter_before = new_letter_generate_inside_function("+�����������������������������-"); break; //�
        case 22: letter_before = new_letter_generate_inside_function("+��������������������������������-"); break; //�
        case 23: letter_before = new_letter_generate_inside_function("+��������������������������������-"); break; //�
        case 24: letter_before = new_letter_generate_inside_function("+������������������������-"); break; //�
        case 25: letter_before = new_letter_generate_inside_function("+������������������������-"); break; //�
        case 26: letter_before = new_letter_generate_inside_function("+������������������������-"); break; //�
        case 27: letter_before = new_letter_generate_inside_function("+������������������������-"); break; //�
        case 28: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�
        case 29: letter_before = new_letter_generate_inside_function("+����������������������-"); break; //�
        case 30: letter_before = new_letter_generate_inside_function("+������������������������������-"); break; //�

        case 31: letter_before = new_letter_generate_inside_function("+�����������������������������-"); break; //�
        case 32: letter_before = new_letter_generate_inside_function("+������������������������-"); break; //�
        case 33: letter_before = new_letter_generate_inside_function("+������������������������-"); break; //�
    }
    return letter_before;
}

string word_main_generate (string letter_before, string word, int word_length) {
    int i = 0;
    while (i < word_length) {
        letter_before = new_letter_generate_outside_function(letter_before);

        word = word + letter_before;
        i++;
    }
    return word;
}

string generate_word_block (int word_length) {
    string word = "";
    word = word_head_generate() + word_main_generate(word_head_generate(), word, word_length);
    return word;
}

bool letter_correcter (string word, int letter_num) {
    string alf_glas = "+���������-";
    int alf_glas_length = string_length_counter(alf_glas);
    int c = 0;

    while (alf_glas[c] != word[letter_num]) {
        if (c == alf_glas_length) {
            return false;
        }
        c++;
    }
    return true;
}

bool word_test (string word, int word_length) {
    int glas_counter = 0;
    int c_glas_counter = 0;

    int c = 0;

    //cout << word << endl;

    while (c != word_length) {
        if (letter_correcter(word, c) == true) {
            glas_counter ++;
            c_glas_counter = 0;
            //cout << glas_counter << " ; " << c_glas_counter << " true" << endl;
        }
        else {
            glas_counter = 0;
            c_glas_counter ++;
            //cout << glas_counter << " ; " << c_glas_counter << " false"  << endl;
        }
        if (glas_counter == 2 || c_glas_counter == 2) {
            return false;
        }
        c++;
    }
    return true;
}

string word_generate () {
    int word_length = rand() % 4 + 4;
    string word = generate_word_block(word_length);

    while (word_test(word, word_length) == false) {
        word = generate_word_block(word_length);
    }
    return word;
}

string generate_word () {
    string word = word_generate();
    return word;
}
