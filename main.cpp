#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <cstring>

using namespace std;


string generate_word();

int main()
{
    setlocale(LC_CTYPE, "rus");
    srand( time(0) );

    int c = 1;
    while (c != 0) {
        cout << generate_word() << endl;
        cout << generate_word() << endl;
        cout << generate_word() << endl;
        cout << generate_word() << endl;
        cout << generate_word() << endl;
        cin >> c;
    }


    return 0;
}
